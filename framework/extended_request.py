import requests
import logging


class ExtendedRequest:
    def __init__(self, response: requests.models.Response):
        self.response = response

    def should_have_status_code(self, status_code):
        status_code_from_request = self.response.json().get('_meta').get('code')
        try:
            assert status_code_from_request == status_code
        except AssertionError:
            print(f'wrong status code. Expected: {status_code}, Actual:{status_code_from_request}')
            raise AssertionError

    def should_have_json_value(self):
        # todo
        pass
