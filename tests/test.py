import pytest

from helpers.requests_helper import Request
from helpers.headers_helper import Headers
from user_data.user import User


class Helper:
    def __init__(self):
        self.api_helper = Request()
        self.headers = Headers()
        self.user = User()

    def get_all_users(self):
        return self.api_helper.send_get_request(url=self.user.users,
                                                headers=self.headers.main_header())


@pytest.fixture(scope="session")
def helper():
    return Helper()


def test_my_first_test(helper: Helper):
    resp = helper.get_all_users()
    resp.should_have_status_code(200)