import requests
from framework.extended_request import ExtendedRequest


class Request:
    def send_get_request(self, url, headers=None):
        try:
            response = requests.get(url=url, params=None, headers=headers)
            return ExtendedRequest(response)
        except Exception as exception:
            print(f"there was an exception during request. exception is {exception}")

    def send_post_request(self, url, json, headers=None):
        try:
            return requests.post(url=url, json=json, params=None, headers=headers)
        except Exception as exception:
            print(f"there was an exception during request. exception is {exception}")

    def send_delete_request(self, url, headers=None):
        try:
            return requests.delete(url=url, params=None, headers=headers)
        except Exception as exception:
            print(f"there was an exception during request. exception is {exception}")

    def send_put_request(self, url, json, headers=None):
        try:
            return requests.put(url=url, data=json, params=None, headers=headers)
        except Exception as exception:
            print(f"there was an exception during request. exception is {exception}")

    def send_patch_request(self, url, json, headers=None):
        try:
            return requests.patch(url=url, data=json, params=None, headers=headers)
        except Exception as exception:
            print(f"there was an exception during request. exception is {exception}")






